//
//  JHSUViewController.h
//  emailApp
//
//  Created by Jennifer Hsu on 5/9/13.
//  Copyright (c) 2013 Jennifer Hsu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import <QuartzCore/QuartzCore.h>
#import <OpenGLES/EAGLDrawable.h>


@interface JHSUViewController : UIViewController <MFMailComposeViewControllerDelegate>

// email button callback function
- ( IBAction ) emailButton:( UIButton * )sender;
// camera button callback function
- ( IBAction )captureScreen:( UIButton * )sender;
// capture screen image
- ( UIImage * )getGLImage;
// capture screen image
- ( UIImage * )getNonGLImage;

@end
