//
//  JHSUViewController.m
//  emailApp
//
//  Created by Jennifer Hsu on 5/9/13.
//  Copyright (c) 2013 Jennifer Hsu. All rights reserved.
//

#import "JHSUViewController.h"

@interface JHSUViewController ()

@end

@implementation JHSUViewController


- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

// button callback function
- ( IBAction )emailButton:( UIButton * )sender
{
    if( [MFMailComposeViewController canSendMail] )
    {
        // init, alloc, and setup
        MFMailComposeViewController * email = [[MFMailComposeViewController alloc] init];
        email.mailComposeDelegate = self;
        
        // create email body
        NSString * emailBody = @"Hi, I'm the body of the email. Look at me.";
        
        // set mail fields
        [email setSubject: @"Hello, from me!"];
        [email setToRecipients: [NSArray arrayWithObjects:@"jhsu@ccrma.stanford.edu", nil]];
        [email setMessageBody:emailBody isHTML:NO];
        
        // attachment addon
        // find the correct path
        NSString * docDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
        NSString * imagePath = [NSString stringWithFormat:@"%@/test.png", docDir];
        // get the image
        UIImage * image = [UIImage imageWithContentsOfFile:imagePath];
        NSData * imageData = UIImagePNGRepresentation(image);
        // attach it
        [email addAttachmentData:imageData mimeType:@"image/png" fileName:@"test"];
        
        // present the email view
        [self presentViewController:email animated:YES completion:nil];
        
        // clean up
        [email release];
        
        
    } else{
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Oops, sorry!"
                                                         message:@"Your device doesn't support the email composer sheet."
                                                        delegate:nil
                                               cancelButtonTitle:@"Ok"
                                               otherButtonTitles:nil];
        [alert show];
        [alert release];
    }
}

// dismiss email interface
- ( void )mailComposeController:( MFMailComposeViewController * )controller didFinishWithResult:( MFMailComposeResult )result error:( NSError * )error
{
    // print out log of what happened
    switch( result )
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved!");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent. Great job!");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Uh oh. The mail failed :(");
            break;
        default:
            NSLog(@"Um...no mail sent. Sorry?");
            break;
    }
    
    // remove mail view
    [self dismissViewControllerAnimated:YES completion:nil];
}

- ( IBAction )captureScreen:( UIButton * )sender
{
    // capture the screen (OpenGLES stuff included...I hope)
    // UIImage * image = [self getGLImage];
    UIImage * image = [self getNonGLImage];
    
    // get our captured image as a png
    NSData * imagePNG = [NSData dataWithData:UIImagePNGRepresentation( image )];
    
    // find the correct path to save to
    NSString *docDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *pngFilePath = [NSString stringWithFormat:@"%@/test.png", docDir];
    
    // save to sandbox
    [imagePNG writeToFile:pngFilePath atomically:YES];
    
    
}

- (UIImage *)getNonGLImage
{
    CGRect screenRect = [[UIScreen mainScreen] bounds];
        
    UIGraphicsBeginImageContext(screenRect.size);
        
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    [[UIColor blackColor] set];
    CGContextFillRect(ctx, screenRect);
        
    [self.view.layer renderInContext:ctx];
    
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
        
    return newImage;
}


- ( UIImage * )getGLImage
{
 
    GLint width;
    GLint height;
    
    width = self.view.frame.size.width;
    height = self.view.frame.size.height;
    
    //glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &width);
    //glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &height);
    
    NSLog(@"%d %d", width, height);
    
    NSInteger myDataLength = width * height * 4;
    
    // allocate array and read pixels into it.
    GLubyte *buffer = (GLubyte *) malloc(myDataLength);
    glReadPixels(0, 0, width, height, GL_RGBA, GL_UNSIGNED_BYTE, buffer);
    
    // gl renders "upside down" so swap top to bottom into new array.
    GLubyte *buffer2 = (GLubyte *) malloc(myDataLength);
    for(int y = 0; y < height; y++)
    {
        for(int x = 0; x < width * 4; x++)
        {
            buffer2[((height - 1) - y) * width * 4 + x] = buffer[y * 4 * width + x];
        }
    }
    
    // make data provider with data.
    CGDataProviderRef provider = CGDataProviderCreateWithData(NULL, buffer2, myDataLength, NULL);
    
    // prep the ingredients
    int bitsPerComponent = 8;
    int bitsPerPixel = 32;
    int bytesPerRow = 4 * width;
    CGColorSpaceRef colorSpaceRef = CGColorSpaceCreateDeviceRGB();
    CGBitmapInfo bitmapInfo = kCGBitmapByteOrderDefault;
    CGColorRenderingIntent renderingIntent = kCGRenderingIntentDefault;
    
    // make the cgimage
    CGImageRef imageRef = CGImageCreate(width, height, bitsPerComponent, bitsPerPixel, bytesPerRow, colorSpaceRef, bitmapInfo, provider, NULL, NO, renderingIntent);
    
    // then make the uiimage from that
    UIImage *myImage = [UIImage imageWithCGImage:imageRef];
    CGImageRelease(imageRef);
    CGDataProviderRelease(provider);
    CGColorSpaceRelease(colorSpaceRef);
    free(buffer);
    free(buffer2);
    return myImage;
    
    
}

@end
