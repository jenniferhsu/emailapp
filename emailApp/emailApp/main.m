//
//  main.m
//  emailApp
//
//  Created by Jennifer Hsu on 5/9/13.
//  Copyright (c) 2013 Jennifer Hsu. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "JHSUAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([JHSUAppDelegate class]));
    }
}
