//
//  JHSUAppDelegate.h
//  emailApp
//
//  Created by Jennifer Hsu on 5/9/13.
//  Copyright (c) 2013 Jennifer Hsu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JHSUAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
